"use strict";
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import {
    getStorage,
    ref as refStorage,
    uploadBytes,
    getDownloadURL,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDV0J1gLfc57rbHkD2kf1bbFD_Sqstsdm8",
    authDomain: "proyecto-2be09.firebaseapp.com",
    databaseURL: "https://proyecto-2be09-default-rtdb.firebaseio.com",
    projectId: "proyecto-2be09",
    storageBucket: "proyecto-2be09.appspot.com",
    messagingSenderId: "443953670989",
    appId: "1:443953670989:web:46d34c01666836d9bae504"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const storage = getStorage();
const db = getDatabase();
const auth = getAuth(app);


// Declarar elementos del DOM
const btnAgregar = document.querySelector("#btnAgregar");
const btnConsultar = document.querySelector("#btnConsultar");
const btnActualizar = document.querySelector("#btnActualizar");
const btnEliminar = document.querySelector("#btnEliminar");
const btnMostrar = document.querySelector("#btnMostrar");
const btnLimpiar = document.querySelector("#btnLimpiar");
const lista = document.querySelector("#lista");
const imagen = document.querySelector("#imagen");
var salir = document.querySelector("#salir");

// Variables input
const getInputs = () => {
    event.preventDefault();

    return {
        codigo: document.querySelector("#codigo").value.trim().toUpperCase(),
        nombre: document.querySelector("#nombre").value.trim(),
        descripcion: document.querySelector("#descripcion").value.trim(),
        precio: document.querySelector("#precio").value.trim(),
    };
};

const clearInputs = () => {
    event.preventDefault();
    document.querySelector("#codigo").value = "";
    document.querySelector("#nombre").value = "";
    document.querySelector("#descripcion").value = "";
    document.querySelector("#precio").value = "";
    document.querySelector("#url").value = "";
    document.querySelector("#imgPreview").src = "#";
    lista.innerHTML = "";
    lista.classList.add("none");
    imagen.value = "";
    document.getElementById("imgPreview").classList.add("none");
};

const fillInputs = ({ codigo, nombre, descripcion, precio, url }) => {
    document.querySelector("#codigo").value = codigo;
    document.querySelector("#nombre").value = nombre;
    document.querySelector("#descripcion").value = descripcion;
    document.querySelector("#precio").value = precio;
    document.querySelector("#imgPreview").src = url;
    document.getElementById("imgPreview").classList.remove("none");
    document.getElementById("url").value=url;
};

const imagenChange = () => {
    document.getElementById("imgPreview").src = URL.createObjectURL(imagen.files[0]);
    document.getElementById("imgPreview").classList.remove("none");
};

async function insertData() {
    try {
        let { codigo, nombre, descripcion, precio } = getInputs();
        const dbref = ref(db);
        const storageRef = refStorage(storage, "productos/" + codigo);

        if (codigo === "" || nombre === "" || descripcion === "" || precio === "" || imagen.value === "") {
            return alert("Existen campos vacios");
        }

        const snapshot = await get(child(dbref, "productos/" + codigo));
        if (snapshot.exists()) return alert("El producto ya existe");

        await uploadBytes(storageRef, imagen.files[0]);
        let url = await getDownloadURL(storageRef);

        await set(ref(db, "productos/" + codigo), {
            nombre,
            descripcion,
            precio,
            url, status: "0"
        });
        alert("Se insertaron con éxito los datos");
        clearInputs();
    } catch (error) {
        console.log(error);
    }
}

async function showData() {
    try {
        let { codigo } = getInputs();
        if (codigo === "") return alert("Introduzca un codigo");

        const dbref = ref(db);
        const snapshot = await get(child(dbref, "productos/" + codigo));

        if (snapshot.exists()) {
            let nombre = snapshot.val().nombre;
            let descripcion = snapshot.val().descripcion;
            let precio = snapshot.val().precio;
            let url = snapshot.val().url;
            fillInputs({ codigo, nombre, descripcion, precio, url });
        } else {
            alert("No se encontró el registro");
        }
    } catch (error) {
        console.log(error);
    }
}

async function showProducts() {
    try {
        const dbref = ref(db, "productos");

        await onValue(dbref, snapshot => {
            lista.innerHTML = `<thead><tr>
                <th width="5%">Código</th>
                <th width="30%">Marca</th>
                <th width="30%">Descripción</th>
                <th width="15%">Precio</th>
                <th width="15%">Imagen</th>
                <th width="5%">Estado</th>
                </tr></thead><tbody></tbody>`;

            snapshot.forEach(childSnapshot => {
                const childKey = childSnapshot.key;
                const childData = childSnapshot.val();
                let imgURL;
                if (childData.url === undefined) {
                    imgURL = urlDefault;
                } else {
                    imgURL = childData.url;
                }
                lista.lastElementChild.innerHTML += `<tr>
					<th>${childKey}</th>
					<td>${childData.nombre}</td>
					<td>${childData.descripcion}</td>
					<td>${childData.precio}</td>
					<td><img class="w-100" src="${imgURL}" alt="Imagen de ${childData.nombre}"/></td>
					<td>${childData.status}</td
				</tr>`;
            });
        });

        lista.classList.remove("none");
    } catch (error) {
        console.log(error);
    }
}

async function updateData() {
	try {
		event.preventDefault();

		let { codigo, nombre, descripcion, precio} = getInputs();
		const storageRef = refStorage(storage, "productos/" + codigo);

		if (isNaN(parseFloat(precio)) || parseFloat(precio) <= 0) return alert("Introduzca un precio valido", "Error");
		if (!codigo || !nombre || !descripcion) return alert("Existen campos vacios");

		if (!imagen.value) {
			await update(ref(db, "productos/" + codigo), { nombre, descripcion, precio });
			return alert("Se realizó una actualización");
		}

		await uploadBytes(storageRef, imagen.files[0]);
		let url = await getDownloadURL(storageRef);

		await update(ref(db, "productos/" + codigo), { nombre, descripcion, precio: "$", url });
		return alert("Se realizó una actualización");
	} catch (error) {
		console.error(error);
	}
}

async function disabledData() {
    try {
		event.preventDefault();

		let { codigo } = getInputs();
		if (codigo === "") return alert("Introduzca un código");
		const dbref = ref(db);

		const snapshot = await get(child(dbref, "productos/" + codigo));
		if (!snapshot.exists()) {
			return alert("No existe un producto con ese código");
		}

		if (snapshot.val().status === "1") {
			await update(ref(db, "productos/" + codigo), { status: "0" });
			alert("El registro fue habilitado");
		} else {
			await update(ref(db, "productos/" + codigo), { status: "1" });
			alert("El registro fue deshabilitado");
		}

		await showProducts();
	} catch (error) {
		console.error(error);
	}
}

onAuthStateChanged(auth, async user => {
	if (user) {
		if (window.location.pathname === "/html/login.html") {
			window.location.href = "/html/admin.html";
		}
	} else {
		if (window.location.pathname === "/html/admin.html") {
			window.location.href = "/html/login.html";
		}
	}
});

btnAgregar.addEventListener("click", insertData);
btnConsultar.addEventListener("click", showData);
btnActualizar.addEventListener("click", updateData);
btnEliminar.addEventListener("click", disabledData);
btnMostrar.addEventListener("click", showProducts);
btnLimpiar.addEventListener("click", clearInputs);
imagen.addEventListener("change", imagenChange);

//cerrar sesión
salir.addEventListener("click", async e => {
	e.preventDefault();
	await signOut(auth);
});

