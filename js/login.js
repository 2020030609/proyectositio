// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDV0J1gLfc57rbHkD2kf1bbFD_Sqstsdm8",
    authDomain: "proyecto-2be09.firebaseapp.com",
    databaseURL: "https://proyecto-2be09-default-rtdb.firebaseio.com",
    projectId: "proyecto-2be09",
    storageBucket: "proyecto-2be09.appspot.com",
    messagingSenderId: "443953670989",
    appId: "1:443953670989:web:46d34c01666836d9bae504"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

var iniciar = document.querySelector("#iniciar");

async function iniciarSesion() {
	event.preventDefault();
	let email = document.getElementById("correo").value;
	let pass = document.getElementById("password").value;

	if (email == "" || pass == "") {
		alert("Todos los campos deben estar llenados");
		return;
	}
	
	try {
		await signInWithEmailAndPassword(auth, email, pass)
        // Signed in
		alert("Bienvenido " + email);
        window.location.href = "admin.html";
	} catch (error) {
		window.location.href = "error.html";
	}
}

onAuthStateChanged(auth, async user => {
	if (user) {
		if (window.location.pathname === "/html/login.html") {
			window.location.href = "/html/admin.html";
		}
	} else {
		if (window.location.pathname === "/html/admin.html") {
			window.location.href = "/html/login.html";
		}
	}
});

iniciar.addEventListener("click", iniciarSesion);

