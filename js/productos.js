"use strict";
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { ref as refStorage, getStorage, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDV0J1gLfc57rbHkD2kf1bbFD_Sqstsdm8",
    authDomain: "proyecto-2be09.firebaseapp.com",
    databaseURL: "https://proyecto-2be09-default-rtdb.firebaseio.com",
    projectId: "proyecto-2be09",
    storageBucket: "proyecto-2be09.appspot.com",
    messagingSenderId: "443953670989",
    appId: "1:443953670989:web:46d34c01666836d9bae504"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const products = document.querySelector("#contenido");
const db = getDatabase();
const storage = getStorage();

async function showProducts() {
    try {
        event.preventDefault();

        products.innerHTML = "";
        const dbref = ref(db, "productos");

        await onValue(dbref, snapshot => {
            snapshot.forEach(childSnapshot => {
                const childData = childSnapshot.val();

                if (childData.status === "0") {

                    products.innerHTML += `
                    <div>
                        <img width="100" height="100" src="${childData.url}">
                        <p class="descripcion">${childData.nombre}</p>
                        <p class="descripcion">${childData.descripcion}</p>
                        <p class="precio">$${childData.precio}</p>
                        <input class="boton" type="submit" value="Comprar">
                    </div>`;
                }
            });
        });

        products.classList.remove("d-none");
    } catch (error) {
        console.error(error);
    }
}

window.addEventListener("DOMContentLoaded", showProducts);
